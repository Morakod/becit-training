﻿using BECIT.NEOS.Admin.BusinessLogic;
using BECIT.NEOS.Admin.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BECIT.NEOS.Admin.aspx
{
    public partial class UserReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BindData();
            }

        }

        private void BindData()
        {
            if (!(Request["p"] == null || Request["p"] == ""))
            {
                string jsonQueryString = Request["p"];
                string jsonString = HttpUtility.UrlDecode(jsonQueryString);
                var filterUserObj = JsonConvert.DeserializeObject<UserFilterModel>(jsonString);

                USERID.Text = filterUserObj.USERID;
                FNAME.Text = filterUserObj.FNAME;
                LNAME.Text = filterUserObj.LNAME;
                lastloginFrom.Text = filterUserObj.lastloginFrom.ToString();
                lastloginTo.Text = filterUserObj.lastloginTo.ToString();
            }

            var reportSource = new UserBL().getAll(null);
            string rdlcfileName = Server.MapPath("~/Report/RDLC/UserReport.rdlc");
            this.ReportViewer1.Reset();
            this.ReportViewer1.LocalReport.Dispose();
            this.ReportViewer1.LocalReport.DataSources.Clear();

            this.ReportViewer1.LocalReport.ReportPath = rdlcfileName;
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", reportSource));
            this.ReportViewer1.Visible = true;
            //this.ReportViewer1.LocalReport.Refresh();
            this.ReportViewer1.ShowBackButton = false;
            this.ReportViewer1.ShowFindControls = false;
            this.ReportViewer1.ShowParameterPrompts = false;
            this.ReportViewer1.ShowPrintButton = false;
            this.ReportViewer1.ShowZoomControl = true;
            this.ReportViewer1.ShowRefreshButton = false;
            //this.ReportViewer1.ZoomMode = Microsoft.Reporting.WebForms.ZoomMode.PageWidth;
            this.ReportViewer1.DataBind();
        }

    }
}