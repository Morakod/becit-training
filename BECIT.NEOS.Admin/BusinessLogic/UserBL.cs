﻿using BECIT.NEOS.Admin.Models;
using BECIT.NEOS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BECIT.NEOS.Admin.BusinessLogic
{
    public class UserBL
    {
        public List<UserModel> getAll(string keyword)
        {
            //using (EntitiesNeosDb db = new EntitiesNeosDb())
            //{
            //    var objModel1 = (from q in db.APPUSER.SqlQuery("SELECT * FROM APPUSER WHERE USERID LIKE '%" + keyword + "%' ")
            //                    select new UserModel
            //                    {
            //                        USERID = q.USERID,
            //                        TITLE = q.TITLE,
            //                        FNAME = q.FNAME,
            //                        LNAME = q.LNAME,
            //                        lastlogin = q.LASTLOGIN,
            //                        AppUserRoleCode = q.APPUSERROLECODE,
            //                        AppUserRoleDesc = q.APPUSERROLE.DESCRIPTION
            //                    }).ToList();
            //}

            using (EntitiesNeosDb db = new EntitiesNeosDb())
            {
                //var objModel = (from q in db.APPUSER select q).ToList();
                var objModel = (from q in db.APPUSER
                                select new UserModel
                                {
                                    USERID = q.USERID,
                                    TITLE = q.TITLE,
                                    FNAME = q.FNAME,
                                    LNAME = q.LNAME,
                                    lastlogin = q.LASTLOGIN,
                                    AppUserRoleCode = q.APPUSERROLECODE,
                                    AppUserRoleDesc = q.APPUSERROLE.DESCRIPTION
                                }).ToList();

                objModel = keyword == null || keyword == "" ? objModel : objModel.Where(x => x.USERID.ToLower().Contains(keyword.ToLower()) || x.FNAME.ToLower().Contains(keyword.ToLower())).ToList();
                return objModel;

            }
        }
    }
}