﻿using BECIT.NEOS.Admin.Models;
using BECIT.NEOS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace BECIT.NEOS.Admin.BusinessLogic
{
    public class CurrentUserBL
    {
        string SuperAdminRoleCode = "SUPERADMIN";
        string msgMemberNotFound = "ไม่พบชื่อผู้ใช้งานระบบ";
        string msgWrongPassword = "รหัสผ่านไม่ถูกต้อง";
        string msgMemberIsLocked = "ผู้ใช้งานนี้ถูกยกเลิกการใช้งาน";

        public CurrentUserModel Verify(string userid, string password)
        {

            //int allowLoginAttemptNumber = 100;


            var test = GetMd5Hash(userid + password);

            CurrentUserModel currentUser = new CurrentUserModel();
            using (EntitiesNeosDb db = new EntitiesNeosDb())
            {
                try
                {
                    var user = (from m in db.APPUSER where m.USERID.Equals(userid) select m).SingleOrDefault();
                    if (user != null)
                    {
                        if (user.ISACTIVE.HasValue && user.ISACTIVE == 1)
                        {
                            if (CheckPassword(user.PASSWORD, user.USERID + password))
                            {
                                currentUser.userid = user.USERID;
                                currentUser.title = user.TITLE;
                                currentUser.fname = user.FNAME;
                                currentUser.lname = user.LNAME;
                                currentUser.lastLogin = user.LASTLOGIN;
                                currentUser.HasPermission = true;

                                List<AppModuleModel> appModules = new List<AppModuleModel>();

                                if (user.APPUSERROLECODE.Equals(SuperAdminRoleCode))
                                {
                                    appModules = (from q in db.APPMODULE
                                                  select new AppModuleModel
                                                  {
                                                      Code = q.CODE,
                                                      ModuleName = q.MODULENAME,
                                                      Description = q.DESCRIPTION,
                                                      ControllerName = q.CONTROLLERNAME,
                                                      ActionName = q.ACTIONNAME,
                                                      Icon = q.ICON,
                                                      ItemOrder = q.ITEMORDER,
                                                      IsActive = q.ISACTIVE,
                                                      AppModuleCode = q.APPMODULECODE
                                                  }
                                        ).ToList();
                                }
                                else
                                {
                                    appModules = (from q in db.APPMODULE
                                                  where q.APPUSERROLE.Contains(user.APPUSERROLE)
                                                  select new AppModuleModel
                                                  {
                                                      Code = q.CODE,
                                                      ModuleName = q.MODULENAME,
                                                      Description = q.DESCRIPTION,
                                                      ControllerName = q.CONTROLLERNAME,
                                                      ActionName = q.ACTIONNAME,
                                                      Icon = q.ICON,
                                                      ItemOrder = q.ITEMORDER,
                                                      IsActive = q.ISACTIVE,
                                                      AppModuleCode = q.APPMODULECODE
                                                  }
                                        ).ToList();

                                }

                                currentUser.AppModule = appModules;

                                user.LASTLOGIN = DateTime.Now;
                                db.SaveChanges();

                            } // end if verify user password
                            else
                            {
                                currentUser.HasPermission = false;
                                currentUser.LoginNotifymessage = msgWrongPassword;

                            }
                        } // End if user isActive
                        else
                        {
                            currentUser.HasPermission = false;
                            currentUser.LoginNotifymessage = msgMemberIsLocked;
                        }
                    }// End if user != null
                    else
                    {
                        currentUser.HasPermission = false;
                        currentUser.LoginNotifymessage = msgMemberNotFound;
                    }
                } // end try
                catch (Exception exc)
                {
                    while (exc.InnerException != null) exc = exc.InnerException;
                    currentUser.HasPermission = false;
                    currentUser.LoginNotifymessage = exc.Message;
                    //throw;
                }

            }
            return currentUser;
        }


        private string GetMd5Hash(string input)
        {
            MD5 md5Hash = MD5.Create();
            string specialChr = "@#$!#";
            input = specialChr + input + specialChr;
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            //return sBuilder.ToString().Substring(0, 20);
            return sBuilder.ToString();
        }

        private bool CheckPassword(string source, string stringToCheck)
        {

            string hashStringToCheck = GetMd5Hash(stringToCheck);

            //return true;

            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(source, hashStringToCheck))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
