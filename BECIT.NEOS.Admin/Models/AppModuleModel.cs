﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BECIT.NEOS.Admin.Models
{
    public class AppModuleModel
    {

            public string Code { get; set; }
            public string ModuleName { get; set; }
            public string Description { get; set; }
            public string ControllerName { get; set; }
            public string ActionName { get; set; }
            public string Icon { get; set; }
            public Nullable<decimal> ItemOrder { get; set; }
            public Nullable<short> IsActive { get; set; }
            public string AppModuleCode { get; set; }
        

    }
}