﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BECIT.NEOS.Admin.Models
{
    public class UserFilterModel
    {
        public string USERID { get; set; }
        public string FNAME { get; set; }
        public string LNAME { get; set; }
        public DateTime? lastloginFrom { get; set; }
        public DateTime? lastloginTo { get; set; }
    }
}