﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BECIT.NEOS.Admin.Models
{
    public class RegisterModel
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
    }
}