﻿using BECIT.NEOS.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BECIT.NEOS.Admin.Models
{
    public class CurrentUserModel
    {
        public string userid { get; set; }
        public string title { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public DateTime? lastLogin { get; set; }
        public string AppUserRoleCode { get; set; }
        public string AppUserRoleDesc { get; set; }
        public bool HasPermission { get; set; }
        public string LoginNotifymessage { get; set; }
        public List<AppModuleModel> AppModule { get; set; }
    }

}