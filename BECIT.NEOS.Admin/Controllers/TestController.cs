﻿using BECIT.NEOS.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BECIT.NEOS.Admin.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /Test/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string fname,string lname,string email,string dummy)
        {
            ViewBag.fname = fname;
            ViewBag.lname = lname;
            ViewBag.email = email;
            return View();
        }

        public ActionResult Register()
        {
            var modelObject = new RegisterModel();
            return View(modelObject);
        }

         [HttpPost]
        public ActionResult Register(RegisterModel modelObject)
        {
            //ViewBag.fname = modelObject.fname;
            //ViewBag.lname = modelObject.lname;
            //ViewBag.email = modelObject.email;
            //return View();

            return View(modelObject);
        }

        public ActionResult Register2()
         {
             var modelObject = new RegisterModel();
             return View(modelObject);
         }

        [HttpPost]
        public ActionResult Register2(RegisterModel modelObject)
        {
            return View(modelObject);
        }

    }
}
