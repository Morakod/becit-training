﻿using BECIT.NEOS.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BECIT.NEOS.Admin.Controllers
{
    public class _BaseController : Controller
    {

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.Controller.GetType().Name.ToLower().Replace("controller", "");

            if (Session["CurrentUser"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                        { "Controller", "Login" },
                        { "Action", "Index" }
                    });
            }
            else
            {
                CurrentUserModel currentUser = (CurrentUserModel)Session["CurrentUser"];
                var AuthorizeControllers = currentUser.AppModule.Where(x => x.ControllerName != null).Select(x => x.ControllerName.ToLower());
                if (AuthorizeControllers.Contains(controllerName) == false)
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                        { "Controller", "Login" },
                        { "Action", "Index" }
                    });
                }

            }

            base.OnActionExecuting(filterContext);
        }
    }
}
