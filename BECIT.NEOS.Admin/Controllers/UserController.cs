﻿using BECIT.NEOS.Admin.Models;
using BECIT.NEOS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace BECIT.NEOS.Admin.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/

        //public ActionResult Index(string keyword)
        //{
        //    using(EntitiesNeosDb db = new EntitiesNeosDb())
        //    {
        //        //var objModel = (from q in db.APPUSER select q).ToList();
        //        var objModel = (from q in db.APPUSER 
        //                        select new UserModel 
        //                        { 
        //                            USERID = q.USERID, 
        //                            TITLE = q.TITLE,
        //                            FNAME = q.FNAME,
        //                            LNAME = q.LNAME,
        //                            lastlogin  = q.LASTLOGIN,
        //                            AppUserRoleCode = q.APPUSERROLECODE,
        //                            AppUserRoleDesc = q.APPUSERROLE.DESCRIPTION
        //                        }).ToList();

        //        //objModel = objModel.Where(x => x.USERID.Contains(keyword) || x.FNAME.Contains(keyword)).ToList();
        //        objModel = keyword == null || keyword == "" ? objModel : objModel.Where(x => x.USERID.ToLower().Contains(keyword.ToLower()) || x.FNAME.ToLower().Contains(keyword.ToLower())).ToList();

        //        ViewBag.keyword = keyword;

        //        return View(objModel);
        //    }
           
        //}

        public ActionResult Index(int? page, string keyword)
        {
            ViewBag.currentAppModuleCode = "DEMOUSER";
            using (EntitiesNeosDb db = new EntitiesNeosDb())
            {
                //var objModel = (from q in db.APPUSER select q).ToList();
                var objModel = (from q in db.APPUSER
                                select new UserModel
                                {
                                    USERID = q.USERID,
                                    TITLE = q.TITLE,
                                    FNAME = q.FNAME,
                                    LNAME = q.LNAME,
                                    lastlogin = q.LASTLOGIN,
                                    AppUserRoleCode = q.APPUSERROLECODE,
                                    AppUserRoleDesc = q.APPUSERROLE.DESCRIPTION
                                }).ToList();

                //objModel = objModel.Where(x => x.USERID.Contains(keyword) || x.FNAME.Contains(keyword)).ToList();
                objModel = keyword == null || keyword == "" ? objModel : objModel.Where(x => x.USERID.ToLower().Contains(keyword.ToLower()) || x.FNAME.ToLower().Contains(keyword.ToLower())).ToList();
                ViewBag.keyword = keyword;

                //paging code
                int pageSize = 2;
                int pageNumber = (page ?? 1);
                var listPaged = objModel.ToPagedList(pageNumber,pageSize);
                
                //end paging code

                return View(listPaged);
            }

        }

        [HttpPost]
        public ActionResult Index(string keyword,string dummy)
        {
            ViewBag.currentAppModuleCode = "DEMOUSER";
            return Index(1,keyword);

        }


        public ActionResult InputForm(string id)
        {
            ViewBag.currentAppModuleCode = "DEMOUSER";
            var objModel = new UserModel();
            if(id != null)
            {
                using (EntitiesNeosDb db = new EntitiesNeosDb())
                {
                    var dbObj = (from q in db.APPUSER where q.USERID == id select q).FirstOrDefault();
                    objModel.USERID = dbObj.USERID;
                    objModel.TITLE = dbObj.TITLE;
                    objModel.FNAME = dbObj.FNAME;
                    objModel.LNAME = dbObj.LNAME;
                    objModel.EMAIL = dbObj.EMAIL;
                    objModel.AppUserRoleCode = dbObj.APPUSERROLECODE;
                }

            }
            return View(objModel);
        }
       
        [HttpPost]
        public ActionResult InputForm(UserModel objModel)
        {

            ViewBag.currentAppModuleCode = "DEMOUSER";
                //Insert into database
            using (EntitiesNeosDb db = new EntitiesNeosDb())
            {
                try
                {
                    var countExistRecord = (from q in db.APPUSER where q.USERID == objModel.USERID select q).Count();
                    if (countExistRecord == 0)
                    {

                        var dbObject = new APPUSER();
                        dbObject.USERID = objModel.USERID;
                        dbObject.TITLE = objModel.TITLE;
                        dbObject.FNAME = objModel.FNAME;
                        dbObject.LNAME = objModel.LNAME;
                        dbObject.EMAIL = objModel.EMAIL;
                        dbObject.APPUSERROLECODE = objModel.AppUserRoleCode;

                        db.APPUSER.Add(dbObject);
                        db.SaveChanges();
                    }
                    else
                    {
                        var existObject = (from q in db.APPUSER where q.USERID == objModel.USERID select q).FirstOrDefault();
                        existObject.TITLE = objModel.TITLE;
                        existObject.FNAME = objModel.FNAME;
                        existObject.LNAME = objModel.LNAME;
                        existObject.EMAIL = objModel.EMAIL;
                        existObject.APPUSERROLECODE = objModel.AppUserRoleCode;
                        db.SaveChanges();
                    }

                    var messageResult = new MessageResultModel();
                    messageResult.Styte = "alert alert-success";
                    messageResult.Title = "Succes!!!";
                    messageResult.Description = "บันทึกข้อมุลเรียบร้อยแล้ว";
                    TempData["messageResult"] = messageResult;
                }
                catch (Exception exc)
                {
                    while (exc.InnerException != null) exc = exc.InnerException;
                    var messageResult = new MessageResultModel();
                    messageResult.Styte = "alert alert-danger";
                    messageResult.Title = "Error!!!";
                    messageResult.Description = "ไม่สามารถบันทึกข้อมุล " + exc.Message;
                    TempData["messageResult"] = messageResult;
                }
            }
            
            return View(objModel);

        }

    }
}
