﻿using BECIT.NEOS.Admin.Models;
using BECIT.NEOS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using BECIT.NEOS.Admin.BusinessLogic;

namespace BECIT.NEOS.Admin.Controllers
{
    public class UserAjaxController : _BaseController
    {
        //
        // GET: /UserAjax/

        public ActionResult Index(int? page, string keyword)
        {
            ViewBag.currentAppModuleCode = "DEMOUSERAJAX";
            var objModel = new UserBL().getAll(keyword);
            
            //using (EntitiesNeosDb db = new EntitiesNeosDb())
            //{
            //    //var objModel = (from q in db.APPUSER select q).ToList();
            //    var objModel = (from q in db.APPUSER
            //                    select new UserModel
            //                    {
            //                        USERID = q.USERID,
            //                        TITLE = q.TITLE,
            //                        FNAME = q.FNAME,
            //                        LNAME = q.LNAME,
            //                        lastlogin = q.LASTLOGIN,
            //                        AppUserRoleCode = q.APPUSERROLECODE,
            //                        AppUserRoleDesc = q.APPUSERROLE.DESCRIPTION
            //                    }).ToList();

            //    //objModel = objModel.Where(x => x.USERID.Contains(keyword) || x.FNAME.Contains(keyword)).ToList();
            //    objModel = keyword == null || keyword == "" ? objModel : objModel.Where(x => x.USERID.ToLower().Contains(keyword.ToLower()) || x.FNAME.ToLower().Contains(keyword.ToLower())).ToList();
                ViewBag.keyword = keyword;

                //paging code
                int pageSize = 2;
                int pageNumber = (page ?? 1);
                var listPaged = objModel.ToPagedList(pageNumber, pageSize);

                //end paging code

                ViewBag.pagedListObj = listPaged;


                return Request.IsAjaxRequest() ? (ActionResult)PartialView("Table") : View();
            //}

        }

        [HttpPost]
        public ActionResult Index(string keyword)
        {
            ViewBag.currentAppModuleCode = "DEMOUSERAJAX";
            return Index(null, keyword);

        }

    }
}
