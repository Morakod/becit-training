﻿using BECIT.NEOS.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BECIT.NEOS.Admin.Controllers
{
    public class UserProfileController : Controller
    {
        //
        // GET: /UserProfile/

        public ActionResult Index()
        {
            CurrentUserModel currentUser = (CurrentUserModel)Session["CurrentUser"];

            ViewBag.userid = currentUser.userid;
            ViewBag.title = currentUser.title;
            ViewBag.fname = currentUser.fname;
            ViewBag.lname = currentUser.lname;
            ViewBag.lastLogin = currentUser.lastLogin;

            //TempData["messageResult"]
            return View();
        }

    }
}
