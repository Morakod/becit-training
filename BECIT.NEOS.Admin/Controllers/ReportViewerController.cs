﻿using BECIT.NEOS.Admin.BusinessLogic;
using BECIT.NEOS.Admin.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BECIT.NEOS.Admin.Controllers
{
    public class ReportViewerController : Controller
    {
        //
        // GET: /ReportViewer/

        public ActionResult Index()
        {
            var objModel = new UserFilterModel();
            return View(objModel);
        }

        [HttpPost]
        public ActionResult Index(UserFilterModel objModel)
        {
            string jsonString = JsonConvert.SerializeObject(objModel);
            string jsonQueryString = HttpUtility.UrlEncode(jsonString);
            ViewBag.jsonQueryString = jsonQueryString;
            return View(objModel);
        }

    }
}
